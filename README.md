# Trax Puzzles

I have collected a lot of trax puzzles.  
However most of them have been unpublished because
I didn't had the time to check my solutions.

But now I decided to them on-line and ask people to check my solutions so please
raise an issue if you find any errors in the solutions.

# About this puzzles

For all problems:
  * It is white to move and win the game
  * The trax variant is looptrax
